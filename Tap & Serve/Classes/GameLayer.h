#ifndef __GAMELAYER_H__
#define __GAMELAYER_H__

#include "AppDelegate.h"
#include "Constants.h"
#include "SimpleAudioEngine.h"
#include "cocos2d.h"
#include "GameOverLayer.h"
#include "WinLayer.h"
#include "PauseLayer.h"
#include "Table.h"

enum GameState
{
	kGameStarting = 0,
	kGamePreparing = 1,
	kGameReady = 2,
	kGamePause = 3,
	kGameFinish = 4,
	kGameEnd = 5
};

class GameLayer : public cocos2d::CCLayer
{
public:
	GameLayer(GameMode gameMode = GameModeHome, GameLevel gameLevel = GameLevelNone);
	virtual ~GameLayer(){};

	void onEnterTransitionDidFinish();
	void configureGame(GameLevel gameLevel);
	void update(float dt);
	void Score(Table* table, SelectedFood orderFood, float time);
	void SetDissatisfaction(int dissatisfaction);
	virtual bool init();
	virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

	cocos2d::CCMenu*      GetMenu()const;

	GameLevel	GetDifficulty()const;

private:
	GameState _gameState;
	GameState _previousGameState;
	GameMode _gameMode;
	GameLevel _gameLevel;
	GameOverLayer* _gameOverLayer;
	WinLayer*		_winLayer;
	PauseLayer* _pauseLayer;
	cocos2d::CCMenu* menu;

	cocos2d::CCMenuItemToggle* _soju;
	cocos2d::CCMenuItemToggle* _beer;
	cocos2d::CCMenuItemToggle* _makgeolli;
	cocos2d::CCMenuItemToggle* _kimchi;
	cocos2d::CCMenuItemToggle* _gimbap;

	cocos2d::Label*	_tipsScore;
	cocos2d::Label*	_tipsLabel;
	cocos2d::Label*	_timerScore;
	cocos2d::Label*	_timerLabel;
	cocos2d::Label* _dissatisfactionLabel;
	cocos2d::Label* _dissatisfactionScore;

	cocos2d::CCMenuItemImage*	_menuPause;

	cocos2d::CCSprite*	_leftBorder;
	cocos2d::CCSprite*	_upBorder;

	std::vector<Table*>*	_tableList;

	SelectedFood _selectedFood;

	void ChangeFood(cocos2d::CCObject* pSender);
	void TickTock(float dt);
	void InitLayers();
	void CreateTable(GameLevel difficulty);
	void PlayAgain(cocos2d::CCObject *pSender);
	void GoHome(cocos2d::CCObject *pSender);
	void PauseGame(cocos2d::CCObject *pSender);
	void PauseAllActions();
	void ResumeGame(cocos2d::CCObject *pSender);
	void ResumeAllActions();
	void SetScore(float score);


	float _timer;
	bool _gameOver;
	bool _win;
	bool _pause;

	float _score;
	int _dissatisfaction;
};

#endif