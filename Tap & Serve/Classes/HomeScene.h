#ifndef __HOMESCENE_H__
#define __HOMESCENE_H__

#include "cocos2d.h"
#include "AppDelegate.h"

class HomeScene : public cocos2d::CCScene
{
private :
	HomeScene(GameMode gameMode, GameLevel gameLevel = GameLevelNone);

public:
	static HomeScene *scene(GameMode gameMode, GameLevel gameLevel = GameLevelNone);
};

#endif