#include "Database.h"
#include "Constants.h"

using namespace cocos2d;

Database::Database() {}

CCUserDefault*	Database::defaults()
{
	return CCUserDefault::sharedUserDefault();
}

float Database::getBestScore()
{
	return defaults()->getFloatForKey(USER_BEST_SCORE, 0.0f);
}

void Database::setBestScore(float score)
{
	defaults()->setFloatForKey(USER_BEST_SCORE, score);
	defaults()->flush();
}

int Database::getTotalGamesPlayed()
{
	return defaults()->getIntegerForKey(USER_TOTAL_GAMES_PLAYED, 0);
}

void Database::incrementTotalGamesPlayed()
{
	defaults()->setIntegerForKey(USER_TOTAL_GAMES_PLAYED, getTotalGamesPlayed() + 1);
	defaults()->flush();
}

float Database::getTotalScore()
{
	return defaults()->getFloatForKey(USER_TOTAL_SCORE, 0.0f);
}

void Database::setTotalScore(float score)
{
	defaults()->setFloatForKey(USER_TOTAL_SCORE, getTotalScore() + score);
	defaults()->flush();

	incrementTotalGamesPlayed();
}

void Database::setScore(float score)
{
	if (score > getBestScore())
		setBestScore(score);
	setTotalScore(score);
	setRank(getTotalScore());
}

bool Database::isMute()
{
	return defaults()->getBoolForKey(USER_MANAGE_MUSIC, false);
}

void Database::setMute(bool state)
{
	defaults()->setBoolForKey(USER_MANAGE_MUSIC, state);
	defaults()->flush();
}

void Database::setRank(int totalScore)
{
	if (totalScore >= 0 && totalScore <= 100)
		defaults()->setStringForKey(USER_RANK, "Trainee");
	else if (totalScore <= 500)
		defaults()->setStringForKey(USER_RANK, "Employee");
	else if (totalScore <= 1000)
		defaults()->setStringForKey(USER_RANK, "Trainer");
	else if (totalScore <= 2000)
		defaults()->setStringForKey(USER_RANK, "Chief");
	else
		defaults()->setStringForKey(USER_RANK, "Big Boss");
	defaults()->flush();
}

std::string Database::getRank()
{
	return defaults()->getStringForKey(USER_RANK, "Unemployed");
}