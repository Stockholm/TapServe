#include "PauseLayer.h"
#include "Constants.h"
#include "HomeScene.h"
#include "GameLayer.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;

#define BACKGROUND_PAUSE "background_pause.jpg"

PauseLayer::PauseLayer()
{
	if (initWithColor(ccc4BFromccc4F(ccc4f(0, 0, 0, 100.0f / 255.0f))))
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint visibleOrigin = CCDirector::sharedDirector()->getVisibleOrigin();
		CCPoint origin = ccp(visibleOrigin.x + visibleSize.width * 0.5f, visibleOrigin.y + visibleSize.height* 0.5f);

		CCSprite* background = CCSprite::create(BACKGROUND_PAUSE);
		background->setScale(0.5f);
		background->setPosition(origin);
		addChild(background);

		CCPoint mid = ccp(background->getContentSize().width / 2, background->getContentSize().height / 2);



		// Play again button
		CCMenuItemImage* itemPlayAgain = CCMenuItemImage::create("pause_replay_off.png", "pause_replay.png", this, menu_selector(PauseLayer::OptionPressed));
		itemPlayAgain->setTag(PausePlayAgain);
		itemPlayAgain->setScale(2);
		itemPlayAgain->setPositionX(mid.x - itemPlayAgain->getContentSize().width * 2);
		itemPlayAgain->setPositionY(mid.y - itemPlayAgain->getContentSize().height * 2.5f);

		CCMenuItemImage* itemResume = CCMenuItemImage::create("pause_play_off.png", "pause_play.png", this, menu_selector(PauseLayer::OptionPressed));
		itemResume->setTag(PauseResumeGame);
		itemResume->setScale(2);
		itemResume->setPositionX(itemPlayAgain->getPositionX() + itemResume->getContentSize().width * 2);
		itemResume->setPositionY(itemPlayAgain->getPositionY());

		CCMenuItemImage* itemHome = CCMenuItemImage::create("pause_home_off.png", "pause_home.png", this, menu_selector(PauseLayer::OptionPressed));
		itemHome->setTag(PauseGoHome);
		itemHome->setScale(2);
		itemHome->setPositionX(itemResume->getPositionX() + itemResume->getContentSize().width * 2);
		itemHome->setPositionY(mid.y - itemHome->getContentSize().height * 2.5f);

		CCMenu* menu = CCMenu::create();
		menu->setAnchorPoint(ccp(0, 0));
		menu->setPosition(CCPoint((float)(0), (float)(0)));
		menu->addChild(itemHome);
		menu->addChild(itemPlayAgain);
		menu->addChild(itemResume);

		background->addChild(menu);
	}
}

void PauseLayer::OptionPressed(CCObject *pSender)
{
	CCMenuItem* item = (CCMenuItem*)pSender;
	SimpleAudioEngine::sharedEngine()->playEffect(SFX_BUTTON);

	switch (item->getTag())
	{
	case PauseGoHome:
		CCNotificationCenter::sharedNotificationCenter()->postNotification(NOTIFICATION_GO_HOME);
		break;
	case PausePlayAgain:
		CCNotificationCenter::sharedNotificationCenter()->postNotification(NOTIFICATION_PLAY_AGAIN);
		break;
	case PauseResumeGame:
		CCNotificationCenter::sharedNotificationCenter()->postNotification(NOTIFICATION_RESUME_GAME);
		break;
	}
}