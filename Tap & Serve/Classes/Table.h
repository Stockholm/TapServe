#ifndef TABLE_H_
# define TABLE_H_

#include "cocos2d.h"
#include "Constants.h"
//#include "2d\CCNode.h"

class GameLayer;

class Table: public cocos2d::CCObject
{
public:
	Table(GameLayer* layer, float posX = 0, float posY = 0);
	~Table();

	void						update(float DeltaTime);

	void						Bad();
	void						Nice();
	void						Serve(cocos2d::CCObject *pSender);
	void						HideOrder();

	void						SetHasOrdered(bool hasOrdered);
	void						SetOrdered(SelectedFood);
	void						SetTimeBetweenOrder(float time);
	void						ResetCountdownToServe();

	bool						HasOrdered()const;
	SelectedFood				GetOrdered()const;
	cocos2d::CCMenuItemImage*	GetImage()const;
	float						GetTimeBetweenOrder()const;
	GameLayer*					GetLayer()const;
	cocos2d::CCSprite*			GetOrderSpriteById(const int id)const;
	

private:
	float								_posX;
	float								_posY;

	bool								_hasOrdered;
	cocos2d::CCMenuItemImage*			_sprite;
	std::vector<cocos2d::CCSprite*>*	_orderSpriteList;
	SelectedFood						_orderedFood;

	cocos2d::CCSprite *nice;
	cocos2d::CCSprite *bad;

	float								_countDownOrder;
	float								_timeToServe;
	float								_countDownToServe;

	GameLayer*							_layer;
	int									_dishesNb;
//	CCFadeIn*							_fadeOut;
};

#endif /* TABLE_H_ */
