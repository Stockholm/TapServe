#ifndef __HOMELAYER_H__
#define __HOMELAYER_H__

#include "cocos2d.h"
#include "GameLayer.h"

enum HomeButtons
{
	TagEasyMode = 0,
	TagHardMode = 1,
	TagSurvivalMode = 2
};

class HomeLayer : public cocos2d::CCLayer
{
public:
	HomeLayer(GameLayer *gameLayer);
	virtual ~HomeLayer() {};

private:
	cocos2d::CCSprite* tablero;
	cocos2d::CCSprite* logo;
	cocos2d::CCMenuItemImage* menuItemEasy;
	cocos2d::CCMenuItemImage* menuItemHard;
	cocos2d::CCMenuItemImage* menuItemSurvival;
	cocos2d::CCMenuItemToggle* menuSound;

	void PlayPressed(cocos2d::CCObject* pSender);
	void ManageMusic(cocos2d::CCObject* pSender);

	void _hideToLeft();
	void _hideToRight();

	void _enableButtons();
	void _disableButtons();

	bool disable;
	cocos2d::CCMenu* menu;
	GameLayer* _gameLayer;

	cocos2d::Label*	_gamesPlayedLabel;
	cocos2d::Label*	_scoreLabel;
	cocos2d::Label*	_rankLabel;
};

#endif