#ifndef __PAUSELAYER_H__
#define __PAUSELAYER_H__

#include "cocos2d.h"

enum PauseButtons
{
	PauseResumeGame = 0,
	PauseGoHome = 1,
	PausePlayAgain = 2
};

class PauseLayer : public cocos2d::CCLayerColor
{
public:
	PauseLayer();

private:
	void OptionPressed(cocos2d::CCObject *pSender);
};

#endif