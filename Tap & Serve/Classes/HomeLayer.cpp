#include "HomeLayer.h"
#include "Constants.h"
#include "SimpleAudioEngine.h"
#include "Database.h"

using namespace cocos2d;
using namespace CocosDenshion;

#define HIDE_TIME 0.4f

HomeLayer::HomeLayer(GameLayer *gameLayer) : _gameLayer(gameLayer)
{
	//CCUserDefault::destroyInstance();
	disable = false;

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint visibleOrigin = CCDirector::sharedDirector()->getVisibleOrigin();

	tablero = CCSprite::create("tablero_title.png");
	tablero->setAnchorPoint(CCPoint((float)(0), (float)(0)));
	tablero->setPositionX(visibleOrigin.x);
	tablero->setPositionY(visibleOrigin.y);
	addChild(tablero);

	logo = CCSprite::create("logo.png");
	logo->setPositionX(-logo->getContentSize().width * 1.2f);
	logo->setPositionY(visibleOrigin.y + visibleSize.height * 0.55f);
	logo->setScale(0.7f);

	CCPoint finalPos = ccp(visibleOrigin.x + visibleSize.width * 0.65f, logo->getPositionY());
	CCMoveTo *move1 = CCMoveTo::create(0.9f, finalPos);
	logo->runAction(CCSequence::create(CCDelayTime::create(0.25f), move1, NULL));
	addChild(logo, -1);

	// Sound management
	CCMenuItemImage* menuSoundOn = CCMenuItemImage::create("sound_on_off.png", "sound_on.png", NULL, NULL);
	CCMenuItemImage* menuSoundOff = CCMenuItemImage::create("sound_off_off.png", "sound_off.png", NULL, NULL);

	menuSound = CCMenuItemToggle::createWithTarget(this, menu_selector(HomeLayer::ManageMusic), menuSoundOn, menuSoundOff, NULL);
	menuSound->setPositionX(WIN_SIZE.width - menuSound->getContentSize().width * 0.8f);
	menuSound->setPositionY(visibleOrigin.y + menuSound->getContentSize().height * 0.75f);

	if (Database::isMute())
		menuSound->setSelectedIndex(1);

	float time_dt = 1.3f;
	float scale = 1.05f;

	// Hard Mode
	menuItemHard = CCMenuItemImage::create("hard.png", "hard_off.png", this, menu_selector(HomeLayer::PlayPressed));
	menuItemHard->setTag(TagHardMode);
	menuItemHard->setAnchorPoint(ccp(0, 0));
	menuItemHard->setPositionX(visibleOrigin.x + 25);
	menuItemHard->setPositionY(visibleOrigin.y + menuItemHard->getContentSize().height * 0.5f);
	menuItemHard->runAction(CCRepeatForever::create(CCSequence::create(CCDelayTime::create(2 * time_dt), CCScaleTo::create(0.5f * time_dt, scale), CCScaleTo::create(0.5f * time_dt, 1.0f), CCDelayTime::create(0), NULL)));

	// Easy Mode
	menuItemEasy = CCMenuItemImage::create("easy.png", "easy_off.png", this, menu_selector(HomeLayer::PlayPressed));
	menuItemEasy->setTag(TagEasyMode);
	menuItemEasy->setAnchorPoint(ccp(0, 0));
	menuItemEasy->setPositionX(menuItemHard->getPositionX());
	menuItemEasy->setPositionY(menuItemHard->getPositionY() + menuItemEasy->getContentSize().height * 1.1f);
	menuItemEasy->runAction(CCRepeatForever::create(CCSequence::create(CCDelayTime::create(1 * time_dt), CCScaleTo::create(0.5f * time_dt, scale), CCScaleTo::create(0.5f * time_dt, 1.0f), CCDelayTime::create(1 * time_dt), NULL)));

	// Survival Mode
	menuItemSurvival = CCMenuItemImage::create("survival.png", "survival_off.png", this, menu_selector(HomeLayer::PlayPressed));
	menuItemSurvival->setTag(TagSurvivalMode);
	menuItemSurvival->setAnchorPoint(ccp(0, 0));
	menuItemSurvival->setPositionX(menuItemEasy->getPositionX());
	menuItemSurvival->setPositionY(menuItemEasy->getPositionY() + menuItemSurvival->getContentSize().height);
	menuItemSurvival->runAction(CCRepeatForever::create(CCSequence::create(CCScaleTo::create(0.5f * time_dt, scale), CCScaleTo::create(0.5f * time_dt, 1.0f), CCDelayTime::create(2 * time_dt), NULL)));

	_rankLabel = Label::createWithBMFont("CartonSixBMP.fnt", StringUtils::format("Rank : %s", Database::getRank().c_str()));
	_rankLabel->setAnchorPoint(ccp(0, 0));
	_rankLabel->setScale(0.45f);
	_rankLabel->setPosition(ccp(menuItemEasy->getPositionX(), menuItemSurvival->getPositionY() + _rankLabel->getContentSize().height * 1.5f));
	_rankLabel->setColor(Color3B::BLACK);
	tablero->addChild(_rankLabel);

	_scoreLabel = Label::createWithBMFont("CartonSixBMP.fnt", String::createWithFormat("Total Tips : %d", (int)Database::getTotalScore())->getCString());
	_scoreLabel->setAnchorPoint(ccp(0, 0));
	_scoreLabel->setScale(0.55f);
	_scoreLabel->setPosition(ccp(menuItemEasy->getPositionX(), _rankLabel->getPositionY() + _scoreLabel->getContentSize().height / 1.8f));
	_scoreLabel->setColor(Color3B::BLACK);
	tablero->addChild(_scoreLabel);

	_gamesPlayedLabel = Label::createWithBMFont("CartonSixBMP.fnt", String::createWithFormat("Games Played : %d", (int)Database::getTotalGamesPlayed())->getCString());
	_gamesPlayedLabel->setAnchorPoint(ccp(0, 0));
	_gamesPlayedLabel->setScale(0.65f);
	_gamesPlayedLabel->setPosition(ccp(menuItemEasy->getPositionX(), _scoreLabel->getPositionY() + _gamesPlayedLabel->getContentSize().height / 1.6f));
	_gamesPlayedLabel->setColor(Color3B::BLACK);
	tablero->addChild(_gamesPlayedLabel);

	// Menu
	menu = CCMenu::create();
	menu->setPosition(CCPoint((float)(0), (float)(0)));
	menu->addChild(menuItemEasy);
	menu->addChild(menuItemHard);
	menu->addChild(menuItemSurvival);
	menu->addChild(menuSound);
	addChild(menu);
}

void HomeLayer::ManageMusic(cocos2d::CCObject* pSender)
{
	if (disable)
		return;
	SimpleAudioEngine::sharedEngine()->playEffect(SFX_BUTTON);
	bool state = Database::isMute();
	Database::setMute(!state);
}

void HomeLayer::PlayPressed(CCObject *pSender)
{
	CCMenuItem*	item = (CCMenuItem*)pSender;
	SimpleAudioEngine::sharedEngine()->playEffect(SFX_BUTTON);

	switch (item->getTag())
	{
	case TagEasyMode:
		_gameLayer->configureGame(GameLevelEasy);
		break;
	case TagHardMode:
		_gameLayer->configureGame(GameLevelHard);
		break;
	case TagSurvivalMode:
		_gameLayer->configureGame(GameLevelSurvival);
		break;
	}
	
	_disableButtons();
	disable = true;
	_hideToLeft();
	_hideToRight();
}

void HomeLayer::_hideToLeft()
{
	CCMoveBy* move = CCMoveBy::create(HIDE_TIME, ccp(-WIN_SIZE.width * 0.8f, 0));
	tablero->runAction((CCAction*)move->clone());
	menuItemEasy->runAction((CCAction*)move->clone());
	menuItemHard->runAction((CCAction*)move->clone());
	menuItemSurvival->runAction((CCAction*)move->clone());
}

void HomeLayer::_hideToRight()
{
	CCMoveBy* move = CCMoveBy::create(HIDE_TIME, ccp(WIN_SIZE.width * 0.8f, 0));
	logo->runAction((CCAction*)move->clone());
	menuSound->runAction((CCAction*)move->clone());
}


void HomeLayer::_enableButtons()
{
	for (const auto &child : menu->getChildren())
	{
		CCMenuItem *item = (CCMenuItem*)child;
		item->setEnabled(true);
	}
}

void HomeLayer::_disableButtons()
{
	for (const auto &child : menu->getChildren())
	{
		CCMenuItem *item = (CCMenuItem*)child;
		item->setEnabled(false);
	}
}