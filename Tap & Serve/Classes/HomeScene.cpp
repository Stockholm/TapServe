#include "HomeScene.h"
#include "Constants.h"
#include "HomeLayer.h"

using namespace cocos2d;

HomeScene::HomeScene(GameMode gameMode, GameLevel gameLevel)
{
	GameLayer *gameLayer = new GameLayer(gameMode, gameLevel);
	gameLayer->autorelease();
	addChild(gameLayer, 99);

	if (gameMode == GameModeHome)
	{
		HomeLayer* homeLayer = new HomeLayer(gameLayer);
		homeLayer->setVisible(true);
		homeLayer->autorelease();
		addChild(homeLayer, 999);
	}
}

HomeScene* HomeScene::scene(GameMode gameMode, GameLevel gameLevel)
{
	HomeScene *scene = new HomeScene(gameMode, gameLevel);
	scene->autorelease();
	return scene;
}