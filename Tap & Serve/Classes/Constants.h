//
//  Constants.h
//  BTEndlessTunnel
//
//  Created by NSS on 3/21/14.
//
//

#ifndef BTEndlessTunnel_Constants_h
#define BTEndlessTunnel_Constants_h

#include "cocos2d.h"
#include "AppMacros.h"

enum SelectedFood
{
	Soju = 0,
	Beer = 1,
	Makgeolli = 2,
	Kimchi = 3,
	Gimbap = 4,
	None = 5
};

#define SHOW_FPS true
#define WIN_SIZE CCDirector::sharedDirector()->getWinSize()

// Definición de ID's para Local Storage

#define USER_TOTAL_GAMES_PLAYED "KEY_TOTAL_GAMES_PLAYED"
#define USER_TOTAL_SCORE "KEY_TOTAL_SCORE"
#define USER_BEST_SCORE "KEY_BEST_SCORE"
#define USER_MANAGE_MUSIC "KEY_MUSIC"
#define USER_RANK "KEY_RANK"

// Notifications
#define NOTIFICATION_PAUSE_GAME "NOT_PAUSE_GAME"
#define NOTIFICATION_RESUME_GAME "NOT_RESUME_GAME"
#define NOTIFICATION_PLAY_AGAIN "NOT_PLAY_AGAIN"
#define NOTIFICATION_GO_HOME "NOT_GO_HOME"
#define NOTIFICATION_ENABLE_BUTTONS "NOTIFICATION_ENABLE_BUTTONS"

// Musics and SFX
#define BG_MUSIC "background.mp3"
#define SFX_FIRED "fired.mp3"
#define SFX_VICTORY "victory.mp3"
#define SFX_BUTTON "button.mp3"

// Game Parameters
#define TIME_TO_SERVE 5.0f
#define TIME_MAX_BETWEEN_COMMAND 12
#define MAX_DISSATISFCATION 10

// Music and Sound
#define BG_MUSIC_VOLUME 0.4f

#endif
