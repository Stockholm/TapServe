#include "WinLayer.h"
#include "Constants.h"
#include "HomeScene.h"
#include "GameLayer.h"
#include "SimpleAudioEngine.h"
#include "Database.h"


using namespace cocos2d;
using namespace CocosDenshion;

#define BACKGROUND_WIN "background_win.jpg"

WinLayer::WinLayer()
{
	if (initWithColor(ccc4BFromccc4F(ccc4f(0, 0, 0, 100.0f / 255.0f))))
	{
		_score = 0;
		disable = false;

		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint visibleOrigin = CCDirector::sharedDirector()->getVisibleOrigin();
		CCPoint origin = ccp(visibleOrigin.x + visibleSize.width * 0.5f, visibleOrigin.y + visibleSize.height* 0.5f);

		CCSprite* background = CCSprite::create(BACKGROUND_WIN);
		background->setScale(0.5f);
		background->setPosition(origin);
		addChild(background);

		CCPoint mid = ccp(background->getContentSize().width / 2, background->getContentSize().height / 2);

		_scoreLabel = Label::createWithBMFont("CartonSixBMP.fnt", "Score : ");
		_scoreLabel->setPosition(ccp(mid.x, mid.y + _scoreLabel->getContentSize().height));
		_scoreLabel->setColor(Color3B::BLACK);
		background->addChild(_scoreLabel);

		_maxScoreLabel = Label::createWithBMFont("CartonSixBMP.fnt", "Best : ");
		_maxScoreLabel->setScale(0.75f);
		_maxScoreLabel->setPosition(ccp(_scoreLabel->getPositionX(), _scoreLabel->getPositionY() - _maxScoreLabel->getContentSize().height * 1.5f));
		_maxScoreLabel->setColor(Color3B::BLACK);
		background->addChild(_maxScoreLabel);

		CCMenuItemImage* itemHome = CCMenuItemImage::create("pause_home_off.png", "pause_home.png", this, menu_selector(WinLayer::OptionPressed));
		itemHome->setTag(WGoHome);
		itemHome->setScale(2);
		itemHome->setPositionX(mid.x - itemHome->getContentSize().width * 1.5f);
		itemHome->setPositionY(_maxScoreLabel->getPositionY() - itemHome->getContentSize().height * 2.0f);

		// Play again button
		CCMenuItemImage* itemPlayAgain = CCMenuItemImage::create("pause_replay_off.png", "pause_replay.png", this, menu_selector(WinLayer::OptionPressed));
		itemPlayAgain->setTag(WPlayAgain);
		itemPlayAgain->setScale(2);
		itemPlayAgain->setPositionX(mid.x + itemHome->getContentSize().width * 1.5f);
		itemPlayAgain->setPositionY(_maxScoreLabel->getPositionY() - itemHome->getContentSize().height * 2.0f);


		CCMenu* menu = CCMenu::create();
		menu->setAnchorPoint(ccp(0, 0));
		menu->setPosition(CCPoint((float)(0), (float)(0)));
		menu->addChild(itemHome);
		menu->addChild(itemPlayAgain);

		background->addChild(menu);
	}
}

void WinLayer::UpdateScore(int score)
{
	_scoreLabel->setString(String::createWithFormat("Score : %d W", (int)score)->getCString());
	_maxScoreLabel->setString(String::createWithFormat("Best : %d W", (int)Database::getBestScore())->getCString());
}

void WinLayer::OptionPressed(CCObject *pSender)
{
	if (disable)
		return;

	CCMenuItem* item = (CCMenuItem*)pSender;
	SimpleAudioEngine::sharedEngine()->playEffect(SFX_BUTTON);

	switch (item->getTag())
	{
	case WGoHome:
		CCNotificationCenter::sharedNotificationCenter()->postNotification(NOTIFICATION_GO_HOME);
		break;
	case WPlayAgain:
		CCNotificationCenter::sharedNotificationCenter()->postNotification(NOTIFICATION_PLAY_AGAIN);
		break;
	}
}