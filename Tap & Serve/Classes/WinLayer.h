#ifndef __WINLAYER_H__
#define __WINLAYER_H__

#include "cocos2d.h"

enum WinButtons
{
	WPlayAgain = 0,
	WGoHome = 1
};

class WinLayer : public cocos2d::CCLayerColor
{
public:
	WinLayer();
	void UpdateScore(int score);

private:
	void OptionPressed(cocos2d::CCObject *pSender);

	int _score;

	cocos2d::Label*	_scoreLabel;
	cocos2d::Label*	_maxScoreLabel;
	bool disable;
};

#endif

