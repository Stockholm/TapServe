#ifndef __GAMEOVERLAYER_H__
#define __GAMEOVERLAYER_H__

#include "cocos2d.h"

enum GameOverButtons
{
	PlayAgain = 0,
	GoHome = 1
};

class GameOverLayer : public cocos2d::CCLayerColor
{
public:
	GameOverLayer();


private:
	void OptionPressed(cocos2d::CCObject *pSender);


	int _score;


	cocos2d::CCSprite* _image;
	bool disable;
};

#endif

