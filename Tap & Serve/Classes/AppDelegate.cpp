//
//  BTEndlessTunnelAppDelegate.cpp
//  BTEndlessTunnel
//
//  Created by NSS on 3/21/14.
//  Copyright __MyCompanyName__ 2014. All rights reserved.
//

#include "AppDelegate.h"
#include "SimpleAudioEngine.h"

#include "Constants.h"
#include "HomeScene.h"
#include <CCGLView.h>
#include <vector>
#include <string>

USING_NS_CC;
using namespace CocosDenshion;
using namespace std;

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
	CCEGLView* pEglView = pDirector->getOpenGLView();
	if (!pEglView)
	{
		pEglView = GLViewImpl::create("Tap & Serve");
		pDirector->setOpenGLView(pEglView);
	}
    
    // Set resolution size and paths
    pEglView->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, kResolutionNoBorder);
    CCSize frameSize = pEglView->getFrameSize();
    
    vector<string> searchPaths;
    
    if(frameSize.height > smallResource.size.height)
    {
        searchPaths.push_back(mediumResource.directory);
        pDirector->setContentScaleFactor(MIN(mediumResource.size.height/designResolutionSize.height, mediumResource.size.width/designResolutionSize.width));
    }
    else
    {
        searchPaths.push_back(smallResource.directory);
        pDirector->setContentScaleFactor(MIN(smallResource.size.height/designResolutionSize.height, smallResource.size.width/designResolutionSize.width));
    }
    
    searchPaths.push_back(mediumResource.directory);
    pDirector->setContentScaleFactor(MIN(mediumResource.size.height/designResolutionSize.height, mediumResource.size.width/designResolutionSize.width));
    
    searchPaths.push_back("audio");
    CCFileUtils::sharedFileUtils()->setSearchPaths(searchPaths);
    
    SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic(BG_MUSIC);
    
    SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(BG_MUSIC_VOLUME);
    SimpleAudioEngine::sharedEngine()->setEffectsVolume(1.0f);
    

    // turn on display FPS
    pDirector->setDisplayStats(SHOW_FPS);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    CCScene *pScene = HomeScene::scene(GameModeHome, GameLevelNone);
    // run
    pDirector->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->stopAnimation();
    SimpleAudioEngine::sharedEngine()->pauseAllEffects();
    CCNotificationCenter::sharedNotificationCenter()->postNotification(NOTIFICATION_PAUSE_GAME);
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->startAnimation();
    SimpleAudioEngine::sharedEngine()->resumeAllEffects();
}
