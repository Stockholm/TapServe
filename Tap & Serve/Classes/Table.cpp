#include "Table.h"
#include "Constants.h"
#include "SimpleAudioEngine.h"
#include "GameLayer.h"

#define TABLE "Table.png"
#define SOJU "soju.png"
#define SOJU_SELECTED "soju_selected.png"
#define BEER "beer.png"
#define BEER_SELECTED "beer_selected.png"
#define MAKGEOLLI "makgeolli.png"
#define MAKGEOLLI_SELECTED "makgeolli_selected.png"
#define KIMCHI "kimchi.png"
#define KIMCHI_SELECTED "kimchi_selected.png"
#define GIMBAP "gimbap.png"
#define GIMBAP_SELECTED "gimbap_selected.png"
#define NICE "nice.png"
#define BAD "bad.png"




using namespace cocos2d;
using namespace CocosDenshion;

Table::Table(GameLayer* layer, float posX, float posY) : _layer(layer), _posX(posX), _posY(posY)
{
	_hasOrdered = false;
	_countDownOrder = (rand() % 5);
	_countDownToServe = TIME_TO_SERVE;
	_orderedFood = None;
	_orderSpriteList = new std::vector<CCSprite*>;
	if (_layer)
	{
		if (_layer->GetDifficulty() == GameLevelEasy)
			_dishesNb = 3;
		else
			_dishesNb = 5;
	}
//	_fadeOut = NULL;

	_sprite = CCMenuItemImage::create(TABLE, TABLE, this, menu_selector(Table::Serve));
	_sprite->setPositionX(posX);
	_sprite->setPositionY(posY);
	_sprite->setAnchorPoint(ccp(0.5, 0.5));

	CCSprite* sprite = CCSprite::create(SOJU);
	sprite->setPosition(ccp(this->GetImage()->getPositionX(), this->GetImage()->getPositionY()));
	sprite->setVisible(false);
	_orderSpriteList->push_back(sprite);
	if (_layer)
		_layer->addChild(sprite, 5);

	sprite = CCSprite::create(BEER);
	sprite->setPosition(ccp(this->GetImage()->getPositionX(), this->GetImage()->getPositionY()));
	sprite->setVisible(false);
	_orderSpriteList->push_back(sprite);
	if (_layer)
		_layer->addChild(sprite, 5);

	sprite = CCSprite::create(MAKGEOLLI);
	sprite->setPosition(ccp(this->GetImage()->getPositionX(), this->GetImage()->getPositionY()));
	sprite->setVisible(false);
	_orderSpriteList->push_back(sprite);
	if (_layer)
		_layer->addChild(sprite, 5);

	sprite = CCSprite::create(KIMCHI);
	sprite->setPosition(ccp(this->GetImage()->getPositionX(), this->GetImage()->getPositionY()));
	sprite->setVisible(false);
	_orderSpriteList->push_back(sprite);
	if (_layer)
		_layer->addChild(sprite, 5);

	sprite = CCSprite::create(GIMBAP);
	sprite->setPosition(ccp(this->GetImage()->getPositionX(), this->GetImage()->getPositionY()));
	sprite->setVisible(false);
	_orderSpriteList->push_back(sprite);
	if (_layer)
		_layer->addChild(sprite, 5);
}

Table::~Table()
{
}

void	Table::update(float DeltaTime)
{
	_countDownOrder -= DeltaTime;
	if (_countDownOrder <= 0.0f)
	{
		_orderedFood = (SelectedFood)(rand() % _dishesNb);
		_hasOrdered = true;
		_countDownOrder = (rand() % TIME_MAX_BETWEEN_COMMAND);
		if (_countDownOrder <= TIME_TO_SERVE + 1)
			_countDownOrder = TIME_TO_SERVE + 2;
	}
	if (_hasOrdered)
	{
		_orderSpriteList->at(_orderedFood)->setVisible(true);
		_countDownToServe -= DeltaTime;
		if (_countDownToServe <= 0.0f)
		{
			_orderSpriteList->at(_orderedFood)->setVisible(false);
			ResetCountdownToServe();
			_hasOrdered = false;
			_layer->SetDissatisfaction(rand() % MAX_DISSATISFCATION);
			Bad();
		}
	}
}

void Table::Bad()
{
	bad = CCSprite::create(BAD);
	bad->setAnchorPoint(CCPoint((float)(0.5), (float)(0.5)));
	bad->setRotation(-45);
	bad->setPositionX(_sprite->getPositionX() - _sprite->getContentSize().width / 2);
	bad->setPositionY(_sprite->getPositionY() + _sprite->getContentSize().height / 2);
	_layer->addChild(bad);
	bad->runAction(CCMoveBy::create(1.0f, ccp(-20, 20)));
	bad->runAction(CCFadeOut::create(1.0f));
}

void Table::Nice()
{
	nice = CCSprite::create(NICE);
	nice->setAnchorPoint(CCPoint((float)(0.5), (float)(0.5)));
	nice->setRotation(45);
	nice->setPositionX(_sprite->getPositionX() + _sprite->getContentSize().width / 2);
	nice->setPositionY(_sprite->getPositionY() + _sprite->getContentSize().height / 2);
	_layer->addChild(nice);
	nice->runAction(CCMoveBy::create(1.0f, ccp(20, 20)));
	nice->runAction(CCFadeOut::create(1.0f));
}

void	Table::Serve(CCObject *pSender)
{
	_layer->Score(this, _orderedFood, _countDownToServe);
}

void	Table::HideOrder()
{
	_orderSpriteList->at(_orderedFood)->setVisible(false);
}

void				Table::SetHasOrdered(bool hasOrdered)
{
	_hasOrdered = hasOrdered;
}

void	Table::ResetCountdownToServe()
{
	_countDownToServe = TIME_TO_SERVE;
}

bool				Table::HasOrdered()const { return _hasOrdered; }
SelectedFood		Table::GetOrdered()const { return _orderedFood; }
CCMenuItemImage*	Table::GetImage()const { return _sprite; }
CCSprite*			Table::GetOrderSpriteById(const int id)const { return _orderSpriteList->at(id); }