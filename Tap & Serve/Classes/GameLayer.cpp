#include "GameLayer.h"
#include "HomeScene.h"
#include "Database.h"

#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>

#define BACKGROUND "background_1.jpg"
#define TABLE "joy_L.png"
#define SOJU "soju.png"
#define SOJU_SELECTED "soju_selected.png"
#define BEER "beer.png"
#define BEER_SELECTED "beer_selected.png"
#define MAKGEOLLI "makgeolli.png"
#define MAKGEOLLI_SELECTED "makgeolli_selected.png"
#define KIMCHI "kimchi.png"
#define KIMCHI_SELECTED "kimchi_selected.png"
#define GIMBAP "gimbap.png"
#define GIMBAP_SELECTED "gimbap_selected.png"


#define GAMETIME 60

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

GameLayer::GameLayer(GameMode gameMode, GameLevel gameLevel) : _gameMode(gameMode)
{
	CCSprite*	_background;
	_background = CCSprite::create(BACKGROUND);
	_background->setScaleX(WIN_SIZE.width / _background->getContentSize().width);
	_background->setScaleY(WIN_SIZE.height / _background->getContentSize().height);
	_background->setAnchorPoint(CCPoint((float)(0.5), (float)(0.5)));
	_background->setPosition(WIN_SIZE.width / 2, WIN_SIZE.height / 2);
	addChild(_background);


	CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(GameLayer::PauseGame), NOTIFICATION_PAUSE_GAME, NULL);
	CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(GameLayer::ResumeGame), NOTIFICATION_RESUME_GAME, NULL);
	CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(GameLayer::PlayAgain), NOTIFICATION_PLAY_AGAIN, NULL);
	CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(GameLayer::GoHome), NOTIFICATION_GO_HOME, NULL);

	_gameLevel = gameLevel;

	_gameOver = false;
	_win = false;
	_pause = false;
	_score = 0;
	_dissatisfaction = 0;
	this->scheduleUpdate();
	_tableList = new std::vector<Table*>;
}

void GameLayer::configureGame(GameLevel gameLevel)
{
	CCPoint visOrigin = CCDirector::sharedDirector()->getVisibleOrigin();
	CCSize visSize = CCDirector::sharedDirector()->getVisibleSize();

	_gameLevel = gameLevel;
	_selectedFood = None;

	CCMenuItemImage* soju = CCMenuItemImage::create(SOJU, SOJU, NULL, NULL);
	CCMenuItemImage* sojuSelected = CCMenuItemImage::create(SOJU_SELECTED, SOJU_SELECTED, NULL, NULL);
	_soju = CCMenuItemToggle::createWithTarget(this, menu_selector(GameLayer::ChangeFood), soju, sojuSelected, NULL);
	_soju->setTag(Soju);
	_soju->setAnchorPoint(ccp(0.5, 0.5));
	_soju->setPositionX(60);
	_soju->setPositionY(WIN_SIZE.height - 150);
	CCMenuItemImage* beer = CCMenuItemImage::create(BEER, BEER, NULL, NULL);
	CCMenuItemImage* beerSelected = CCMenuItemImage::create(BEER_SELECTED, BEER_SELECTED, NULL, NULL);
	_beer = CCMenuItemToggle::createWithTarget(this, menu_selector(GameLayer::ChangeFood), beer, beerSelected, NULL);
	_beer->setTag(Beer);
	_beer->setAnchorPoint(ccp(0.5, 0.5));
	_beer->setPositionX(60);
	_beer->setPositionY(WIN_SIZE.height - 240);
	CCMenuItemImage* makgeolli = CCMenuItemImage::create(MAKGEOLLI, MAKGEOLLI, NULL, NULL);
	CCMenuItemImage* makgeolliSelected = CCMenuItemImage::create(MAKGEOLLI_SELECTED, MAKGEOLLI_SELECTED, NULL, NULL);
	_makgeolli = CCMenuItemToggle::createWithTarget(this, menu_selector(GameLayer::ChangeFood), makgeolli, makgeolliSelected, NULL);;
	_makgeolli->setTag(Makgeolli);
	_makgeolli->setAnchorPoint(ccp(0.5, 0.5));
	_makgeolli->setPositionX(60);
	_makgeolli->setPositionY(WIN_SIZE.height - 330);
	if (gameLevel == GameLevelHard || gameLevel == GameLevelSurvival)
	{
		CCMenuItemImage* kimchi = CCMenuItemImage::create(KIMCHI, KIMCHI, NULL, NULL);
		CCMenuItemImage* kimchiSelected = CCMenuItemImage::create(KIMCHI_SELECTED, KIMCHI_SELECTED, NULL, NULL);
		_kimchi = CCMenuItemToggle::createWithTarget(this, menu_selector(GameLayer::ChangeFood), kimchi, kimchiSelected, NULL);;
		_kimchi->setTag(Kimchi);
		_kimchi->setAnchorPoint(ccp(0.5, 0.5));
		_kimchi->setPositionX(60);
		_kimchi->setPositionY(WIN_SIZE.height - 420);
		CCMenuItemImage* gimbap = CCMenuItemImage::create(GIMBAP, GIMBAP, NULL, NULL);
		CCMenuItemImage* gimbapSelected = CCMenuItemImage::create(GIMBAP_SELECTED, GIMBAP_SELECTED, NULL, NULL);
		_gimbap = CCMenuItemToggle::createWithTarget(this, menu_selector(GameLayer::ChangeFood), gimbap, gimbapSelected, NULL);;
		_gimbap->setTag(Gimbap);
		_gimbap->setAnchorPoint(ccp(0.5, 0.5));
		_gimbap->setPositionX(60);
		_gimbap->setPositionY(WIN_SIZE.height - 510);
	}
	else
	{
		_kimchi = NULL;
		_gimbap = NULL;
	}

	menu = CCMenu::create();
	menu->setPosition(CCPoint((float)(0), (float)(0)));
	menu->addChild(_soju);
	menu->addChild(_beer);
	menu->addChild(_makgeolli);
	if (gameLevel == GameLevelHard | gameLevel == GameLevelSurvival)
	{
		menu->addChild(_kimchi);
		menu->addChild(_gimbap);
	}
	addChild(menu);

	CreateTable(gameLevel);

	_tipsLabel = Label::createWithBMFont("CartonSixBMP.fnt", "Tips : ");
	_tipsLabel->setScale(0.5f);
	_tipsLabel->setAnchorPoint(ccp(0.5, 0.5));
	_tipsLabel->setPosition(Vec2(_soju->getPositionX() + 150, _soju->getPositionY() + 20));
	_tipsLabel->setColor(Color3B::BLACK);
	addChild(_tipsLabel);
	_tipsScore = Label::createWithBMFont("CartonSixBMP.fnt", "0");
	_tipsScore->setScale(0.5f);
	_tipsScore->setAnchorPoint(ccp(0.5, 0.5));
	_tipsScore->setPosition(Vec2(_tipsLabel->getPositionX() + 70, _soju->getPositionY() + 20));
	_tipsScore->setColor(Color3B::BLACK);
	addChild(_tipsScore);
	if (gameLevel != GameLevelSurvival)
	{
		_timerLabel = Label::createWithBMFont("CartonSixBMP.fnt", "Time : ");
		_timerLabel->setScale(0.5f);
		_timerLabel->setAnchorPoint(ccp(0.5, 0.5));
		_timerLabel->setPosition(Vec2(_tipsScore->getPositionX() + 150, _soju->getPositionY() + 20));
		_timerLabel->setColor(Color3B::BLACK);
		addChild(_timerLabel);

		this->schedule(schedule_selector(GameLayer::TickTock), 1.0f);
		_timer = GAMETIME;
		_timerScore = Label::createWithBMFont("CartonSixBMP.fnt", String::createWithFormat("%i%", (int)_timer)->getCString());
		_timerScore->setScale(0.5f);
		_timerScore->setAnchorPoint(ccp(0.5, 0.5));
		_timerScore->setPosition(Vec2(_timerLabel->getPositionX() + 70, _soju->getPositionY() + 20));
		_timerScore->setColor(Color3B::BLACK);
		addChild(_timerScore);
	}


	_dissatisfactionLabel = Label::createWithBMFont("CartonSixBMP.fnt", "Dissatisfaction : ");
	_dissatisfactionLabel->setScale(0.5f);
	_dissatisfactionLabel->setAnchorPoint(ccp(0.5, 0.5));
	if (gameLevel == GameLevelSurvival)
		_dissatisfactionLabel->setPosition(Vec2(_tipsScore->getPositionX() + 200, _soju->getPositionY() + 20));
	else
		_dissatisfactionLabel->setPosition(Vec2(_timerScore->getPositionX() + 200, _soju->getPositionY() + 20));
	_dissatisfactionLabel->setColor(Color3B::BLACK);
	addChild(_dissatisfactionLabel);
	_dissatisfactionScore = Label::createWithBMFont("CartonSixBMP.fnt", "0");
	_dissatisfactionScore->setScale(0.5f);
	_dissatisfactionScore->setColor(Color3B::GREEN);
	_dissatisfactionScore->setAnchorPoint(ccp(0.5, 0.5));
	_dissatisfactionScore->setPosition(Vec2(_dissatisfactionLabel->getPositionX() + 150, _soju->getPositionY() + 20));
	addChild(_dissatisfactionScore);

	_menuPause = CCMenuItemImage::create("pause_off.png", "pause.png", this, menu_selector(GameLayer::PauseGame));
	_menuPause->setVisible(true);
	_menuPause->setPositionX(visOrigin.x + _menuPause->getContentSize().width * 0.7f);
	_menuPause->setPositionY(visOrigin.y + _menuPause->getContentSize().width * 0.7);
	CCMenu* menuPause = CCMenu::create();
	menuPause->setAnchorPoint(ccp(0, 0));
	menuPause->setPosition(CCPoint((float)(0), (float)(0)));
	menuPause->addChild(_menuPause);
	addChild(menuPause, 9);

	InitLayers();

	if (!Database::isMute())
		SimpleAudioEngine::sharedEngine()->playBackgroundMusic(BG_MUSIC, true);

	scheduleUpdate();
}

void GameLayer::onEnterTransitionDidFinish()
{
	if (_gameMode == GameModePlay || _gameMode == GameModePlayAgain)
	{
		configureGame(_gameLevel);
	}
}

void	GameLayer::Score(Table* table, SelectedFood orderFood, float timeRemaining)
{
	if (orderFood == _selectedFood && table->HasOrdered() == true)
	{
		SetScore(timeRemaining);
		table->SetHasOrdered(false);
		table->HideOrder();
		table->ResetCountdownToServe();
		table->Nice();
	}
	else
	{
		SetDissatisfaction((rand() % MAX_DISSATISFCATION));
		table->Bad();
	}
}

void GameLayer::ChangeFood(CCObject *pSender)
{
	CCMenuItem*	item = (CCMenuItemToggle*)pSender;
	_selectedFood = (SelectedFood)item->getTag();
	for (const auto &child : menu->getChildren())
	{
		if ((SelectedFood)child->getTag() != _selectedFood)
		{
			CCMenuItemToggle *item = (CCMenuItemToggle*)child;
			item->setSelectedIndex(0);
		}
	}
}

void GameLayer::TickTock(float dt) {
	_timer -= dt;
	_timerScore->setString(String::createWithFormat("%i", (int)_timer)->getCString());
	if (_timer <= 0)
	{
		_win = true;
		_menuPause->setVisible(false);
	}
}

void GameLayer::InitLayers()
{
	_gameOverLayer = new GameOverLayer();
	_gameOverLayer->setPosition(ccp(0, -WIN_SIZE.height));
	_gameOverLayer->autorelease();
	addChild(_gameOverLayer, 10);

	_winLayer = new WinLayer();
	_winLayer->setPosition(ccp(0, -WIN_SIZE.height));
	_winLayer->autorelease();
	addChild(_winLayer, 10);

	_pauseLayer = new PauseLayer();
	_pauseLayer->setVisible(false);
	_pauseLayer->setPosition(ccp(0, -WIN_SIZE.height));
	_pauseLayer->setPositionY(0);
	_pauseLayer->autorelease();
	addChild(_pauseLayer, 9);
}

void	GameLayer::CreateTable(GameLevel difficulty)
{
	Table* table = NULL;
	if (difficulty == GameLevelEasy)
	{
		for (size_t i = 0; i < 2; i++)
		{
			table = new Table(this, (WIN_SIZE.width / 2.5f) + i * 400, (WIN_SIZE.height / 2) - 150);
			_tableList->push_back(table);
			menu->addChild(table->GetImage());
		}
		for (size_t i = 0; i < 2; i++)
		{
			table = new Table(this, (WIN_SIZE.width / 2.5f) + i * 400, (WIN_SIZE.height / 2) + 100);
			_tableList->push_back(table);
			menu->addChild(table->GetImage());
		}
	}
	else
	{
		for (size_t i = 0; i < 3; i++)
		{
			table = new Table(this, (WIN_SIZE.width / 3) + i * 250, (WIN_SIZE.height / 2) - 150);
			_tableList->push_back(table);
			menu->addChild(table->GetImage());
		}
		for (size_t i = 0; i < 3; i++)
		{
			table = new Table(this, (WIN_SIZE.width / 3) + i * 250, (WIN_SIZE.height / 2) + 100);
			_tableList->push_back(table);
			menu->addChild(table->GetImage());
		}
	}
}

void GameLayer::update(float dt)
{
	if (_pause)
		return;

	// update tables
	if (_tableList != NULL)
	{
		for (std::vector<Table*>::iterator it = _tableList->begin(); it != _tableList->end(); ++it)
			(*it)->update(dt);
	}
	if (_win)
	{
		Database::setScore(_score);
		SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
		SimpleAudioEngine::sharedEngine()->playEffect(SFX_VICTORY);
		_winLayer->UpdateScore(_score);
		_winLayer->runAction(CCMoveBy::create(0.25f, ccp(0, WIN_SIZE.height)));
		this->unschedule(schedule_selector(GameLayer::TickTock));
		unscheduleUpdate();
	}
	if (_gameOver)
	{
		Database::setScore(0);
		SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
		SimpleAudioEngine::sharedEngine()->playEffect(SFX_FIRED);
		_gameOverLayer->runAction(CCMoveBy::create(0.25f, ccp(0, WIN_SIZE.height)));
		this->unschedule(schedule_selector(GameLayer::TickTock));
		unscheduleUpdate();
	}
}

void GameLayer::PlayAgain(CCObject	*pSender)
{
	SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	CCScene* scene = HomeScene::scene(GameModePlayAgain, _gameLevel);
	CCDirector::sharedDirector()->replaceScene(CCTransitionFadeDown::create(0.5f, scene));
}

void GameLayer::GoHome(CCObject	*pSender)
{
	SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	CCScene *scene = HomeScene::scene(GameModeHome, GameLevelNone);
	CCDirector::sharedDirector()->replaceScene(CCTransitionFadeDown::create(0.5f, scene));
}

void GameLayer::PauseGame(CCObject	*pSender)
{
	if (!_pause)
	{
		SimpleAudioEngine::sharedEngine()->playEffect(SFX_BUTTON);
		SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();

		_pause = true;
		_previousGameState = _gameState;
		_gameState = kGamePause;

		PauseAllActions();
		pauseSchedulerAndActions();
		

		_menuPause->setVisible(false);
		_pauseLayer->setVisible(true);
	}
}

void GameLayer::PauseAllActions()
{
	for (const auto &child : getChildren())
	{
		child->pauseSchedulerAndActions();
	}
}

void GameLayer::ResumeGame(CCObject	*pSender)
{
	if (_gameState == kGamePause)
	{
		SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
		_gameState = _previousGameState;
		_pauseLayer->setVisible(false);
		_menuPause->setVisible(true);
		resumeSchedulerAndActions();
		ResumeAllActions();
		_pause = false;
	}
}

void GameLayer::ResumeAllActions()
{
	for (const auto &child : getChildren())
	{
		child->resumeSchedulerAndActions();
	}
}

void GameLayer::SetDissatisfaction(int dissatisfaction)
{
	_dissatisfaction += dissatisfaction;
	if (_dissatisfaction <= 75)
		_dissatisfactionScore->setColor(Color3B::GREEN);
	else if (_dissatisfaction < 100)
		_dissatisfactionScore->setColor(Color3B::RED);
	else if (_dissatisfaction >= 100)
	{
		_dissatisfaction = 100;
		(_gameLevel == GameLevelSurvival) ? _win = true : _gameOver = true;
		_menuPause->setVisible(false);
	}
	_dissatisfactionScore->setString(String::createWithFormat("%i", (int)_dissatisfaction)->getCString());
}

void GameLayer::SetScore(float score)
{
	_score += score;
	_tipsScore->setString(String::createWithFormat("%i", (int)_score)->getCString());
}

bool GameLayer::init()
{
	if (CCLayer::init()) 
	{
		this->setKeypadEnabled(true);
		return true;
	}
	return false;
}

void GameLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_BACKSPACE)
	{
		CCDirector::sharedDirector()->stopAnimation();
		SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
		SimpleAudioEngine::sharedEngine()->stopAllEffects();
		CCDirector::sharedDirector()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		exit(0);
#endif
	}
}


CCMenu*	GameLayer::GetMenu()const{ return menu; }
GameLevel	GameLayer::GetDifficulty()const{ return _gameLevel; }
