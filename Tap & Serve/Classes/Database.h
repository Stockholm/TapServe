#ifndef __DATABASE_H__
#define __DATABASE_H__

#include "cocos2d.h"

class Database : public cocos2d::CCObject
{
private:
	Database();
	static cocos2d::CCUserDefault* defaults();

	static void incrementTotalGamesPlayed();
	static void setTotalScore(float score);
	static void setBestScore(float score);

public:
	static float getBestScore();

	static int getTotalGamesPlayed();
	static float getTotalScore();

	static void setScore(float score);

	static void setMute(bool state);
	static bool isMute();

	static void setRank(int totalScore);
	static std::string getRank();
};

#endif