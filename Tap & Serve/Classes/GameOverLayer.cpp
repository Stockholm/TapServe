#include "GameOverLayer.h"
#include "Constants.h"
#include "HomeScene.h"
#include "GameLayer.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;

#define BACKGROUND_GAMEOVER "background_lose.jpg"
#define IMAGE "fired.png"

GameOverLayer::GameOverLayer()
{
	if (initWithColor(ccc4BFromccc4F(ccc4f(0, 0, 0, 100.0f / 255.0f))))
	{
		_score = 0;
		disable = false;

		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint visibleOrigin = CCDirector::sharedDirector()->getVisibleOrigin();
		CCPoint origin = ccp(visibleOrigin.x + visibleSize.width * 0.5f, visibleOrigin.y + visibleSize.height* 0.5f);

		CCSprite* background = CCSprite::create(BACKGROUND_GAMEOVER);
		background->setScale(0.5f);
		background->setPosition(origin);
		addChild(background);

		CCPoint mid = ccp(background->getContentSize().width / 2, background->getContentSize().height / 2);

		_image = CCSprite::create(IMAGE);
		_image->setPosition(ccp(mid.x, mid.y));
		background->addChild(_image, 10);

		CCMenuItemImage* itemHome = CCMenuItemImage::create("pause_home_off.png", "pause_home.png", this, menu_selector(GameOverLayer::OptionPressed));
		itemHome->setTag(GoHome);
		itemHome->setScale(2);
		itemHome->setPositionX(mid.x - itemHome->getContentSize().width * 1.5f);
		itemHome->setPositionY(_image->getPositionY() - itemHome->getContentSize().height * 2.5f);

		// Play again button
		CCMenuItemImage* itemPlayAgain = CCMenuItemImage::create("pause_replay_off.png", "pause_replay.png", this, menu_selector(GameOverLayer::OptionPressed));
		itemPlayAgain->setTag(PlayAgain);
		itemPlayAgain->setScale(2);
		itemPlayAgain->setPositionX(mid.x + itemHome->getContentSize().width * 1.5f);
		itemPlayAgain->setPositionY(_image->getPositionY() - itemHome->getContentSize().height * 2.5f);


		CCMenu* menu = CCMenu::create();
		menu->setAnchorPoint(ccp(0, 0));
		menu->setPosition(CCPoint((float)(0), (float)(0)));
		menu->addChild(itemHome);
		menu->addChild(itemPlayAgain);

		background->addChild(menu);
	}
}

void GameOverLayer::OptionPressed(CCObject *pSender)
{
	if (disable)
		return;

	CCMenuItem* item = (CCMenuItem*)pSender;
	SimpleAudioEngine::sharedEngine()->playEffect(SFX_BUTTON);

	switch (item->getTag())
	{
	case GoHome:
		CCNotificationCenter::sharedNotificationCenter()->postNotification(NOTIFICATION_GO_HOME);
		break;
	case PlayAgain:
		CCNotificationCenter::sharedNotificationCenter()->postNotification(NOTIFICATION_PLAY_AGAIN);
		break;
	}
}